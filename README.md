# README #

Analysis of extra functional properties like performance for general software systems is an important challenge in the field of software engineering. The analysis methods are still limited and rarely used in industry. Nowadays, many social networking applications or online shopping portals have recorded highly fluctuating load. Performance benchmarking after the development of an application results in rework and longer development cycles, thus, increasing the overall cost of the project. Palladio is a method which can be effectively used to do performance analysis at design time for component based architecture. However, self adaptive systems are already available to cater the requirements of fluctuating load for such applications. Therefore, performance analysis at design time of such systems should be done in keeping the dynamically changing behavior of such systems in consideration.

Palladio consists of Palladio Component Model (PCM) that provides a meta model which allows the specification of performance-relevant information of a component based architecture. The special focus of this model is the prediction of QoS attributes. The process of component based software engineering builds complex software systems by breaking the architecture into several components which is developed by different developers having different roles and then assembling the components.


I have used Hadoop MapReduce framework to model it's functionality using PCM and observe it's behavior at runtime.

This repository consists of PCM instance of Hadoop Mapreduce programming model.
It can be imported in Eclipse with Palladio plugin installed from here:

http://www.palladio-simulator.com/tools/features/